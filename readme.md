# Prerequisites
passwordless ssh access to the debian machine  
passwordless sudo on the local machine running this playbook  

# Getting started

1. copy the example files: `cp example/inventory.ini.example inventory.ini` and `cp example/vars.yml.example vars.yml`
2. set your ip or hostname to your debian machine in `inventory.ini`
3. change ansible-user if you created a user other than debian
4. change letsencrypt_email in `vars.yml` to a mail of yours
5. change k3s_config_file in `vars.yml` if you want to use another k3s-config file
   1. keep in mind that some of settings needs to be set to get cilium up and running
6. run the playbook with: `ansible-playbook -i inventory.ini letsencrypt.yml`
7. optional:
   1. set `deploy_test_app: true` in `vars.yml`
   2. set a subdomain pointing to your new k3s cluster in the `subdomain_for_test_app` variable in `vars.yml`
   3. deploy test-app with `kubectl --kubeconfig=kubeconfigs/config apply -f test-app/smoke-test.yml`


# what does it do?

This playbooks creates a single k3s cluster.  
Kube-proxy, flannel-backend, network-policy, servicelb and traefik will be disabled and replaced by cilium.  
Metallb is not needed because cilium will provide the same functionality with l2announcements/bgp and an IPAM. l2announcements will be used in this deployment.